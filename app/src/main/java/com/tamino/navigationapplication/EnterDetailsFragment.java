package com.tamino.navigationapplication;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavDirections;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.tamino.navigationapplication.databinding.FragmentEnterDetailsBinding;


public class EnterDetailsFragment extends Fragment {

    private FragmentEnterDetailsBinding binding;

    public EnterDetailsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = FragmentEnterDetailsBinding.inflate(inflater,container,false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding.btnVerifyDetails.setOnClickListener(v -> enviarDatos(v));
    }

    public void enviarDatos(View v){
        /*esto es posible gracias al plugin safe-args de gradle
         * https://developer.android.com/guide/navigation/navigation-getting-started
         * https://developer.android.com/guide/navigation/navigation-navigate
         * https://developer.android.com/guide/navigation/navigation-pass-data#Safe-args
         * */
        String nombre = binding.etFirstName.getText().toString();
        String mobileNumber = binding.etMobileNumber.getText().toString();

        if(nombre.isEmpty()){
            Toast.makeText(getContext(), "El nombre no puede estar vacio", Toast.LENGTH_SHORT).show();
        }
        else if(mobileNumber.isEmpty()){
            Toast.makeText(getContext(), "El numero no puede estar vacio", Toast.LENGTH_SHORT).show();
        }
        else{
            NavDirections action = EnterDetailsFragmentDirections
                    .actionEnterDetailsFragmentToVerifyDetailsFragment(Integer.parseInt(mobileNumber))
                    .setName(nombre);

            Navigation.findNavController(v).navigate(action);
        }
    }
}