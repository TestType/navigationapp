package com.tamino.navigationapplication;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tamino.navigationapplication.databinding.FragmentVerifyDetailsBinding;


public class VerifyDetailsFragment extends Fragment {

    private FragmentVerifyDetailsBinding binding;

    public VerifyDetailsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState)
    {
        // Inflate the layout for this fragment
        binding = FragmentVerifyDetailsBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        String nombre = VerifyDetailsFragmentArgs.fromBundle(getArguments()).getName();
        int mobileNumber = VerifyDetailsFragmentArgs.fromBundle(getArguments()).getMobileNumber();


        binding.tvName.setText(nombre);
        binding.tvMobileNumber.setText(Integer.toString(mobileNumber));//setText(mobileNumber);
    }
}